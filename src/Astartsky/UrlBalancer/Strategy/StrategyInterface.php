<?php
namespace Astartsky\UrlBalancer\Strategy;

interface StrategyInterface
{
    /**
     * @param string $url
     * @param int $buckets
     * @return int
     */
    public function choose($url, $buckets);
}