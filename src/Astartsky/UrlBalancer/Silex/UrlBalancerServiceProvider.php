<?php
namespace Astartsky\UrlBalancer\Silex;

use Astartsky\UrlBalancer\Domain;
use Astartsky\UrlBalancer\Strategy\HashStrategy;
use Astartsky\UrlBalancer\UrlBalancer;
use Silex\Application;
use Silex\ServiceProviderInterface;

class UrlBalancerServiceProvider implements ServiceProviderInterface
{
    /**
     * Registers services on the given app.
     * @param Application $app An Application instance
     */
    public function register(Application $app)
    {
        $app["url_balancer"] = $app->share(function() use ($app) {
            $balancer = new UrlBalancer();
            $balancer->setStrategy($app["url_balancer_strategy"]);
            foreach ($app["url_balancer_domains"] as $domain) {
                $balancer->addBucket(new Domain($domain));
            }

            return $balancer;
        });

        if (false === isset($app["url_balancer_strategy"])) {
            $app["url_balancer_strategy"] = $app->share(function() use ($app) {
                return new HashStrategy();
            });
        }

        if (false === isset($app["url_balancer_domains"])) {
            $app["url_balancer_domains"] = array();
        }
    }

    /**
     * Bootstraps the application.
     * @param Application $app An Application instance
     */
    public function boot(Application $app)
    {

    }
}
