<?php
namespace Astartsky\UrlBalancer;

use Astartsky\UrlBalancer\Cache\CacheInterface;
use Astartsky\UrlBalancer\Strategy\StrategyInterface;

class UrlBalancer
{
    protected $buckets = array();

    /** @var StrategyInterface */
    protected $strategy;

    /** @var CacheInterface */
    protected $cache;

    /**
     * @param Domain $bucket
     */
    public function addBucket(Domain $bucket)
    {
        $this->buckets[] = $bucket;
    }

    /**
     * @param StrategyInterface $strategy
     */
    public function setStrategy(StrategyInterface $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * @param CacheInterface $cache
     */
    public function setCache(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param string $originUrl
     * @return string
     * @throws Exception
     */
    public function getUrl($originUrl)
    {
        $newUrl = $this->cache ? $this->cache->get($originUrl) : null;

        if (null === $newUrl) {
            $domain = $this->findNewDomain($originUrl);

            if (null === $domain) {
                throw new Exception("Bucket is not found");
            }

            $d = new Url($domain->getDomain());

            $u = new Url($originUrl);
            $u->setScheme($d->getScheme());
            $u->setHost($d->getHost());

            $newUrl = (string) $u;

            if ($this->cache) {
                $this->cache->save($originUrl, $newUrl);
            }
        }

        return $newUrl;
    }

    /**
     * @param string $url
     * @return Domain
     * @throws Exception
     */
    protected function findNewDomain($url)
    {
        $index = null;

        if (null === $this->strategy) {
            throw new Exception("Strategy is not set");
        }

        if (0 === count($this->buckets)) {
            throw new Exception("Buckets are not set");
        }

        if (null === $index) {
            $index = $this->strategy->choose($url, count($this->buckets));
        }

        return isset($this->buckets[$index]) ? $this->buckets[$index] : null;
    }
}
